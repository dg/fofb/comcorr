architecture basic of TestCtrl is

    type arr_UartStimType is array(31 downto 0) of UartStimType;

    signal ConfigDone : integer_barrier := 1 ;
    signal TestDone : integer_barrier := 1 ;

    signal RxStim : arr_UartStimType;

begin

    ------------------------------------------------------------
    -- ControlProc
    --   Set up AlertLog and wait for end of test
    ------------------------------------------------------------
    ControlProc : process
    begin
        -- Initialization of test
        SetAlertLogName("BasicTest");
        SetLogEnable(PASSED, TRUE);    -- Enable PASSED logs
        SetLogEnable(INFO, TRUE);    -- Enable INFO logs

        -- Wait for testbench initialization
        wait for 0 ns ;  wait for 0 ns;
        TranscriptOpen(OSVVM_RESULTS_DIR & "BasicTestTr.txt");
        SetTranscriptMirror(TRUE);

        -- Wait for Design Reset
        wait until nReset = '1';
        ClearAlerts;

        -- Wait for test to finish
        WaitForBarrier(TestDone, 35 ms);
        AlertIf(now >= 35 ms, "Test finished due to timeout");
        AlertIf(GetAffirmCount < 1, "Test is not Self-Checking");


        TranscriptClose;
        EndOfTestReports;
        std.env.stop;
        wait;
    end process ControlProc;


    ------------------------------------------------------------
    -- ManagerProc
    --   Generate transactions for AxiManager
    ------------------------------------------------------------
    ManagerProc : process
        variable Data : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) ;
    begin
        wait until nReset = '1';
        WaitForClock(ManagerRec, 2);

        log("Read version", INFO) ;
        --Write(ManagerRec, X"AAAA_AAA0", X"5555_5555" ) ;
        Read(ManagerRec, std_logic_vector(C_REGISTER_INFO(C_VERSION_ID).address), Data) ;
        AffirmIfEqual(Data, C_VERSION, "Manager Read Data: ") ;

        log("==--- Configure the DUT ---==", INFO);
        log("+-- Global Config", INFO);
        -- Rate to 12,5 Mbps
        Write(ManagerRec, std_logic_vector(C_REGISTER_INFO(C_RATEDIV_ID).address),
                    std_logic_vector(to_unsigned(20, AXI_DATA_WIDTH)));
        -- Period send
        Write(ManagerRec, std_logic_vector(C_REGISTER_INFO(C_PERIOD_ID).address),
                    std_logic_vector(to_unsigned(250, AXI_DATA_WIDTH)));

        log("+-- Line Config", INFO);
        -- Line 0 : PSCID 0x23
        Write(ManagerRec, std_logic_vector(C_REGISTER_INFO(C_BLD_ARR_ID).address), X"00010023") ;
        -- Line 1 : PSCID 0x6A
        Write(ManagerRec, std_logic_vector(C_REGISTER_INFO(C_BLD_ARR_ID).address+4), X"0001006A") ;

        WaitForBarrier(ConfigDone);

        -- Global Enable
        WaitForClock(ManagerRec, 10) ;
        Write(ManagerRec, std_logic_vector(C_REGISTER_INFO(C_CONFIG_ID).address), X"00000001") ;

        -- Wait for outputs to propagate and signal TestDone
        WaitForClock(ManagerRec, 2000) ;
        WaitForBarrier(TestDone) ;
        wait ;
    end process ManagerProc ;

    ------------------------------------------------------------
    -- AxiTransmitterProc
    --   Generate transactions for AxiTransmitter
    ------------------------------------------------------------
    TransmitterProc : process
        variable Data : std_logic_vector(DATA_WIDTH-1 downto 0) ;
        variable OffSet : integer ;
        variable TransactionCount : integer;
        variable ErrorCount : integer;
        variable CurTime : time ;
        variable TxAlertLogID : AlertLogIDType ;

    begin
        wait until nReset = '1' ;
        WaitForClock(StreamTxRec, 2) ;

        WaitForBarrier(ConfigDone) ;

        log("Send word", INFO);
        Data := X"000012340023";
        Send(StreamTxRec, Data);

        WaitForClock(StreamTxRec, 800) ;

        Data := X"000043210023";
        Send(StreamTxRec, Data);

        -- Wait for outputs to propagate and signal TestDone
        WaitForClock(StreamTxRec, 2) ;
        WaitForBarrier(TestDone) ;
        wait ;
    end process TransmitterProc ;

    ------------------------------------------------------------
    -- UartTbRxProc
    --   Gets transactions from UartRx via UartGet and UartCheck
    ------------------------------------------------------------
    gen_proc_uart:for I in 0 to 31 generate
        UartTbRxProc : process
            variable v_RxStim, ExpectStim : UartStimType ;
            variable Available, TryExpectValid : boolean ;

            variable UartRxID : AlertLogIDType ;
            variable TransactionCount, ErrorCount : integer ;
        begin
            wait until nReset = '1' ;

            while True loop
                Get(UartRxRec(I), v_RxStim.Data, v_RxStim.Error) ;
                log("UART #"&to_string(I)&"Received "&to_string(v_RxStim), INFO);
                RxStim(I) <= v_RxStim;

            end loop;

        end process;
    end generate;


end Basic;

Configuration tc_basic of tb_comcorr is
  for TestHarness
    for TestCtrl_1 : TestCtrl
      use entity work.TestCtrl(Basic);
    end for;
  end for;
end tc_basic;

