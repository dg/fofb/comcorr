library osvvm_uart ;
  context osvvm_uart.UartContext ;
-- Not very beautiful...
package pkg_arr_uart is
    type arr_UartRecType is array(31 downto 0) of UartRecType;
end package;

library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;

library OSVVM ;
  context OSVVM.OsvvmContext ;
  use osvvm.ScoreboardPkg_slv.all ;

library osvvm_AXI4 ;
    context osvvm_AXI4.AxiStreamContext ;

library osvvm_Axi4 ;
  context osvvm_Axi4.Axi4LiteContext ;

library osvvm_uart ;
  context osvvm_uart.UartContext ;

library desyrdl;
use desyrdl.pkg_comcorr.all;

use work.pkg_arr_uart.all;

use work.pkg_comcorr_version.all;


entity TestCtrl is
    port (
        -- Global Signal Interface
        Clk                 : In    std_logic ;
        nReset              : In    std_logic ;

        -- Transaction Interfaces
        UartRxRec           : InOut arr_UartRecType;
        StreamTxRec         : inout StreamRecType;
        ManagerRec          : inout AddressBusRecType
    ) ;

    -- Derive AXI interface properties from the StreamTxRec
    constant DATA_WIDTH : integer := StreamTxRec.DataToModel'length ;
    constant DATA_BYTES : integer := DATA_WIDTH/8 ;

    -- Simplifying access to Burst FIFOs using aliases
    --alias TxBurstFifo : ScoreboardIdType is StreamTxRec.BurstFifo ;
    --alias RxBurstFifo : ScoreboardIdType is StreamRxRec.BurstFifo ;

    constant AXI_ADDR_WIDTH : integer := ManagerRec.Address'length ;
    constant AXI_DATA_WIDTH : integer := ManagerRec.DataToModel'length ;

    constant OSVVM_RESULTS_DIR   : string := "" ;
    constant OSVVM_PATH_TO_TESTS : string := "" ;

end entity TestCtrl ;

