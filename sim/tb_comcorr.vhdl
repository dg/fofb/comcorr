library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;
  use ieee.numeric_std_unsigned.all ;

library osvvm ;
    context osvvm.OsvvmContext ;

library osvvm_AXI4 ;
    context osvvm_AXI4.AxiStreamContext ;
    context osvvm_AXI4.Axi4LiteContext ;

library osvvm_uart ;
  context osvvm_uart.UartContext ;

library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_comcorr.all;

use work.pkg_arr_uart.all;
use work.pkg_corrpacket_stream.all;

entity tb_comcorr is
end entity tb_comcorr ;

architecture TestHarness of tb_comcorr is

    constant tperiod_Clk : time := 4 ns ;
    constant tpd         : time := 1 ns ;

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal Clk       : std_logic;
    signal nReset    : std_logic;

    -- Address Bus Transaction Interface
    signal ManagerRec: AddressBusRecType(
      Address(C_ADDR_WIDTH-1 downto 0),
      DataToModel(C_DATA_WIDTH-1 downto 0),
      DataFromModel(C_DATA_WIDTH-1 downto 0)
    ) ;

    -- AXI Manager Functional Interface
    signal   AxiBus : Axi4LiteRecType(
        WriteAddress( Addr (C_ADDR_WIDTH-1 downto 0) ),
        WriteData   ( Data (C_DATA_WIDTH-1 downto 0),   Strb(C_DATA_WIDTH/8-1 downto 0) ),
        ReadAddress ( Addr (C_ADDR_WIDTH-1 downto 0) ),
        ReadData    ( Data (C_DATA_WIDTH-1 downto 0) )
    ) ;

    -- Stream Transaction Interface
    signal StreamTxRec : StreamRecType(
        DataToModel   (C_TDATA_W-1 downto 0),
        DataFromModel (0 downto 0),
        ParamToModel  (3 downto 0),
        ParamFromModel(3 downto 0)
    ) ;

    -- AXI Stream transmitter Functionnal Interface
    signal TValid    : std_logic ;
    signal TReady    : std_logic ;
    signal TData     : std_logic_vector(C_TDATA_W-1 downto 0) ;
    signal TID       : std_logic_vector(0 downto 0) ;
    signal TDest     : std_logic_vector(0 downto 0) ;
    signal TUser     : std_logic_vector(0 downto 0) ;
    signal TStrb     : std_logic_vector(C_TDATA_W/8-1 downto 0) ;
    signal TKeep     : std_logic_vector(C_TDATA_W/8-1 downto 0) ;
    signal TLast     : std_logic ;

    -- UART Transaction interface
    signal UartRxRec : arr_UartRecType;

    -- UART Functionnal interface
    signal UartLines : std_logic_vector(31 downto 0);

    ---------------------------
    -- COMPONENT DECLARATION --
    ---------------------------
    component TestCtrl is
    port (
        -- Global Signal Interface
        Clk             : In    std_logic ;
        nReset          : In    std_logic ;
        -- Transaction Interfaces
        UartRxRec     : InOut arr_UartRecType;
        StreamTxRec   : inout StreamRecType;
        ManagerRec    : inout AddressBusRecType
    );
    end component TestCtrl ;


begin

    ---------------------
    -- CLOCK AND RESET --
    ---------------------
    Osvvm.TbUtilPkg.CreateClock (
        Clk        => Clk,
        Period     => Tperiod_Clk
    );

    Osvvm.TbUtilPkg.CreateReset (
        Reset       => nReset,
        ResetActive => '0',
        Clk         => Clk,
        Period      => 7 * tperiod_Clk,
        tpd         => tpd
    );

    -----------------------
    -- DUT INSTANCIATION --
    -----------------------
    dut:  entity work.top_comcorr
    port map(
        clk                         => Clk,
        rstn                        => nReset,

        -- AXI-MM Status and Config
        s_axi_awaddr                => AxiBus.WriteAddress.Addr,
        s_axi_awprot                => AxiBus.WriteAddress.Prot,
        s_axi_awvalid               => AxiBus.WriteAddress.Valid,
        s_axi_wdata                 => AxiBus.WriteData.Data,
        s_axi_wstrb                 => AxiBus.WriteData.Strb,
        s_axi_wvalid                => AxiBus.WriteData.Valid,
        s_axi_bready                => AxiBus.WriteResponse.Ready,
        s_axi_araddr                => AxiBus.ReadAddress.Addr,
        s_axi_arprot                => AxiBus.ReadAddress.Prot,
        s_axi_arvalid               => AxiBus.ReadAddress.Valid,
        s_axi_rready                => AxiBus.ReadData.Ready,
        s_axi_awready               => AxiBus.WriteAddress.Ready,
        s_axi_wready                => AxiBus.WriteData.Ready,
        s_axi_bresp                 => AxiBus.WriteResponse.Resp,
        s_axi_bvalid                => AxiBus.WriteResponse.Valid,
        s_axi_arready               => AxiBus.ReadAddress.Ready,
        s_axi_rdata                 => AxiBus.ReadData.Data,
        s_axi_rresp                 => AxiBus.ReadData.Resp,
        s_axi_rvalid                => AxiBus.ReadData.Valid,

        -- AXIS BPM com input
        s_axis_tdata                => TData,
        s_axis_tvalid               => TValid,
        s_axis_tready               => TReady,

        period_tick=> open,
        line_out => UartLines


    );

    -----------------------------
    -- VERIFICATION COMPONENTS --
    -----------------------------
    -- AXI-MM Verification Manager
    vc_axi_manager : Axi4LiteManager
    port map (
        -- Globals
        Clk         => Clk,
        nReset      => nReset,
        -- AXI Manager Functional Interface
        AxiBus      => AxiBus,
        -- Testbench Transaction Interface
        TransRec    => ManagerRec
    ) ;

    -- Axi-Stream Verification Manager
    vc_axis_transmitter : AxiStreamTransmitter
    generic map (
        tperiod_Clk    => tperiod_Clk,
        tpd_Clk_TValid => tpd,
        tpd_Clk_TID    => tpd,
        tpd_Clk_TDest  => tpd,
        tpd_Clk_TUser  => tpd,
        tpd_Clk_TData  => tpd,
        tpd_Clk_TStrb  => tpd,
        tpd_Clk_TKeep  => tpd,
        tpd_Clk_TLast  => tpd
    )
    port map (
        -- Globals
        Clk       => Clk,
        nReset    => nReset,
        -- AXI Stream Interface
        TValid    => TValid,
        TReady    => TReady,
        TID       => TID,
        TDest     => TDest,
        TUser     => TUser,
        TData     => TData ,
        TStrb     => TStrb,
        TKeep     => TKeep,
        TLast     => TLast,
        -- Testbench Transaction Interface
        TransRec  => StreamTxRec
    );

    -- Uart Verification manager
    gen_vc_uart: for I in 0 to 31 generate
        vc_uartrx: UartRx
        generic map(
            DEFAULT_BAUD            => (1 ms)/12500,
            DEFAULT_PARITY_MODE     => UARTTB_PARITY_ODD
        )
        port map(
            TransRec => UartRxRec(I),
            SerialDataIn => UartLines(I)
        );
    end generate;


    ---------------
    -- TEST CTRL --
    ---------------
    TestCtrl_1 : TestCtrl
    port map (
        -- Globals
        Clk            => Clk,
        nReset         => nReset,

        -- Testbench Transaction Interfaces
        UartRxRec      => UartRxRec,
        StreamTxRec    => StreamTxRec,
        ManagerRec     => ManagerRec
    ) ;


        end architecture TestHarness;
