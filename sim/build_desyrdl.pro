set desyrdlvhdlpath "/home/broucquart/Projects/CellNode/fwk_cellnode/prj/cellnode_fpga_default/cellnode_fpga_default.desyrdl/vhdl"

library desyrdl
analyze $desyrdlvhdlpath/pkg_desyrdl_common.vhd
analyze $desyrdlvhdlpath/reg_field_storage.vhd
analyze $desyrdlvhdlpath/decoder_axi4l.vhd
analyze $desyrdlvhdlpath/pkg_comcorr.vhd
analyze $desyrdlvhdlpath/comcorr.vhd

