LinkLibraryDirectory /home/broucquart/tmp/OSVVM/sim/VHDL_LIBS/GHDL-3.0.0-dev

include build_desyrdl.pro

library work

analyze ../hdl/pkg_comcorr_version.vhd
analyze ../hdl/pkg_corrframe_stream.vhd
analyze ../hdl/bilt_line_driver.vhd
analyze ../hdl/rate_ticker.vhd
analyze ../hdl/top_comcorr.vhd

analyze TestCtrl_e.vhd
analyze tb_comcorr.vhdl

analyze tc_basic.vhdl

