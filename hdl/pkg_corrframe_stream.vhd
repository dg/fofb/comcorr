-- Package CORRPACKET STREAM
-- this package describe the format of the AXI-Stream interface used by blocs of the module COMCORR.
--
-- The usefull things are :
--   * Two record types for port interfaces: t_corrpacket_axis_m2s and t_corrpacket_axis_s2m.
--   * One record type for frame fields: t_corrpacket.
--   * Two functions to transform TDATA (std_logic_vector) to/from t_corrpacket: slv2corrpacket and corrpacket2slv.
library ieee;
use ieee.std_logic_1164.all;

package pkg_corrpacket_stream is

    ----------------------
    -- MACRO PARAMETERS --
    ----------------------
    constant C_TDEST_W : natural := 7;
    constant C_TDATA_W : natural := 48;

    ---------------------------------
    -- AXIS MASTER/SLAVE INTERFACE --
    ---------------------------------
    type t_corrpacket_axis_m2s is record
        tdest  : std_logic_vector(C_TDEST_W-1 downto 0);
        tdata  : std_logic_vector(C_TDATA_W-1 downto 0);
        tlast  : std_logic;
        tvalid : std_logic;
    end record t_corrpacket_axis_m2s;

    type t_corrpacket_axis_s2m is record
        tready : std_logic;
    end record t_corrpacket_axis_s2m;

    ------------------------
    -- AXIS STREAM PACKET --
    ------------------------
    type t_corrpacket is record
        psc_id                       : std_logic_vector(15 downto 0);
        corr_val                     : std_logic_vector(31 downto 0);
    end record t_corrpacket;

    constant C_CORRPACKET_ZERO : t_corrpacket := (
        psc_id        => (others => '0'),
        corr_val      => (others => '0')
        );

    function slv2corrpacket(
        signal tdata : std_logic_vector(C_TDATA_W-1 downto 0)
        )
        return t_corrpacket;

    function corrpacket2slv(
        signal packet : t_corrpacket
        )
        return std_logic_vector;


end package;

package body pkg_corrpacket_stream is


    function slv2corrpacket(
        signal tdata : std_logic_vector(C_TDATA_W-1 downto 0)
        )
        return t_corrpacket is
        variable packet : t_corrpacket;
    begin
        packet.psc_id       := tdata(15 downto 0);
        packet.corr_val     := tdata(47 downto 16);
        return packet;
    end function;

    function corrpacket2slv(
        signal packet : t_corrpacket
        )
        return std_logic_vector is
    begin
        return packet.corr_val & packet.psc_id;
    end function;

end package body;
