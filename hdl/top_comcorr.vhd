library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_comcorr.all;

use work.pkg_corrpacket_stream.all;
use work.pkg_comcorr_version.all;

entity top_comcorr is
    port(
        clk     : in std_logic;
        rstn    : in std_logic;

        -- AXI-MM Status and Config
        s_axi_m2s                   : in t_comcorr_m2s;
        s_axi_s2m                   : out t_comcorr_s2m;

        -- AXIS corr com input
        s_axis_tdata                : in std_logic_vector(C_TDATA_W-1 downto 0);
        s_axis_tvalid               : in std_logic;

        -- line outputs
        period_tick                 : out std_logic;
        line_out                    : out std_logic_vector(31 downto 0)
    );
end entity top_comcorr;

architecture struct of top_comcorr is

    signal pkt_input           : t_corrpacket;
    signal uart_tick           : std_logic;
    signal s_period_tick       : std_logic;
    signal areset              : std_logic;

    signal addrmap_i           : t_addrmap_comcorr_in;
    signal addrmap_o           : t_addrmap_comcorr_out;

begin

    areset <= not rstn;

    period_tick <= s_period_tick;

    ----------------------
    -- AXI-MM INTERFACE --
    ----------------------
    inst_aximm: entity desyrdl.comcorr
    port map(
        pi_clock    => clk,
        pi_reset    => areset,
        pi_s_top    => s_axi_m2s,
        po_s_top    => s_axi_s2m,
        pi_addrmap  => addrmap_i,
        po_addrmap  => addrmap_o
    );

    addrmap_i.version.data.data <= C_VERSION;

    -----------------
    -- RATE TICKER --
    -----------------
    inst_rate_ticker: entity work.rate_ticker
    port map(
        clk => clk,
        rstn => rstn,
        rate_div => addrmap_o.ratediv.data.data,
        period => addrmap_o.period.data.data,
        ena => addrmap_o.config.en.data(0),
        period_tick => s_period_tick,
        tick => uart_tick
    );

    ------------------
    -- LINE DRIVERS --
    ------------------
    gen_ldr: for I in 0 to 31 generate
        inst_line_driver: entity work.bilt_line_driver
        port map(
            clk     => clk,
            rstn    => rstn,
            uart_tick   => uart_tick,
            period_tick => s_period_tick,
            pkt_valid => s_axis_tvalid,
            pkt_pscid => pkt_input.psc_id,
            pkt_value => pkt_input.corr_val(15 downto 0),
            enable     => addrmap_o.bld_arr(I).ena.data(0),
            pscid      => addrmap_o.bld_arr(I).pscid.data,
            line_out    => line_out(I)
        );

    end generate gen_ldr;

    --------------------
    -- AXIS INTERFACE --
    --------------------
    pkt_input <= slv2corrpacket(s_axis_tdata);

end architecture;
