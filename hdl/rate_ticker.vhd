library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rate_ticker is
    port(
        clk         : in std_logic;
        rstn        : in std_logic;
        rate_div    : in std_logic_vector(31 downto 0);
        period      : in std_logic_vector(31 downto 0);
        ena         : in std_logic;
        period_tick : out std_logic;
        tick        : out std_logic
    );
end entity;

architecture rtl of rate_ticker is

    signal cnt_per      : unsigned(31 downto 0); -- period rate counter
    signal cnt_per_tick : std_logic;
    signal cnt          : unsigned(31 downto 0); -- uart rate counter
    signal cnt_tick     : std_logic; -- uart rate tick

begin


    tick        <= cnt_tick;
    period_tick <= cnt_per_tick;

    p_cnt:process(clk, rstn)
    begin
        if rstn = '0' then
            cnt             <= (others => '0');
            cnt_per         <= (others => '0');
            cnt_tick        <= '0';
            cnt_per_tick    <= '0';

        elsif rising_edge(clk) then

            -- Count when enable
            if ena ='0' then
                cnt_per     <= unsigned(period);
                cnt         <= unsigned(rate_div);
                cnt_tick    <= '0';
                cnt_per_tick<= '0';

            else

                -- uart rate counter
                if cnt = 0 then
                    cnt         <= unsigned(rate_div);
                    cnt_tick    <= '1';
                else
                    cnt         <= cnt-1;
                    cnt_tick    <= '0';
                end if;

                if cnt_per = 0 then
                    cnt_per      <= unsigned(period);
                    cnt_per_tick <= '1';
                else
                    cnt_per     <= cnt_per-1;
                    cnt_per_tick <= '0';
                end if;

            end if;

        end if;
    end process p_cnt;

end architecture;

