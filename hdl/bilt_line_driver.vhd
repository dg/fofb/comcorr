library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.xor_reduce;

entity bilt_line_driver is
    port(
        clk     : in std_logic;
        rstn    : in std_logic;

        uart_tick   : in std_logic;     -- uart symbol tick
        period_tick : in std_logic;

        pkt_valid : in std_logic;   -- input packet is valid
        pkt_pscid :in std_logic_vector(15 downto 0);  -- pscid in packet
        pkt_value :in std_logic_vector(15 downto 0);  -- data in packet

        enable     : in std_logic;     -- enable the line driver
        pscid      : in std_logic_vector(15 downto 0); -- pscid of the line driver

        line_out    : out std_logic
    );
end entity;

architecture rtl of bilt_line_driver is

    -- reg length: 2*(start, stop, byte, parity) + pause = 23
    signal shift_reg : std_logic_vector(23 downto 0);
    signal load_value : std_logic_vector(23 downto 0);
    signal reg_value    : std_logic_vector(15 downto 0);
    signal data_rdy   : std_logic;

    signal load : std_logic;

begin

    ------------
    -- OUTPUT --
    ------------
    line_out    <= (not enable) or shift_reg(0);

    ----------------
    -- ID CHECKER --
    ----------------
    load <= data_rdy and period_tick;

    -------------------
    -- PARALLEL WORD --
    -------------------
    -- Parallel word that'll be serialized
    load_value(0)            <= '1';                                      -- Line pause
    load_value(1)            <= '0';                                      -- Start
    load_value(9 downto 2)   <= reg_value(7 downto 0);                    -- Byte
    load_value(10)            <= xor_reduce(reg_value(7 downto 0) & "1");  -- Parity
    load_value(11)           <= '1';                                      -- Stop
    load_value(12)           <= '1';                                      -- Pause (2nd symbol stops)
    load_value(13)           <= '0';                                      -- Start
    load_value(21 downto 14) <= reg_value(15 downto 8);                   -- Byte
    load_value(22)           <= xor_reduce(reg_value(15 downto 8) & "1"); -- Parity
    load_value(23)           <= '1';                                      -- Stop

    -------------------
    -- DATA REGISTER --
    -------------------
    p_data_reg:process(clk, rstn)
    begin
        if rstn = '0' then
            reg_value   <= (others => '0');
            data_rdy    <= '0';
        elsif rising_edge(clk) then

            -- Register when input data is valid and for this PSCID
            if pkt_valid='1' and pkt_pscid=pscid then
                reg_value <= pkt_value;
                data_rdy <= '1';
            else
                -- Clear ready flag upon data consumption
                if period_tick='1' then
                    data_rdy <= '0';
                end if;
            end if;

        end if;
    end process;

    --------------------
    -- SHIFT REGISTER --
    --------------------
    p_shift_reg:process(clk, rstn)
    begin
        if rstn = '0' then
            shift_reg   <= (others => '1');
        elsif rising_edge(clk) then
            if load = '1' then
                shift_reg <= load_value;
            else
                if uart_tick = '1' then
                    shift_reg   <= "1"&shift_reg(shift_reg'left downto 1);
                end if;
            end if;
        end if;
    end process p_shift_reg;

end architecture;

