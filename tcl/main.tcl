################################################################################
# Main tcl for the module
################################################################################

# ==============================================================================
proc init {} {

    # Configuration

}

# ==============================================================================
proc setSources {} {
  variable Vhdl

  # Generate VHDL package with modversion
  genModVerFile VHDL ../hdl/pkg_comcorr_version.vhd

  lappend Vhdl ../hdl/pkg_comcorr_version.vhd
  lappend Vhdl ../hdl/bilt_line_driver.vhd
  lappend Vhdl ../hdl/top_comcorr.vhd
  lappend Vhdl ../hdl/rate_ticker.vhd
  lappend Vhdl ../hdl/pkg_corrframe_stream.vhd

}

# ==============================================================================
proc setAddressSpace {} {
    variable AddressSpace
    addAddressSpace AddressSpace "comcorr" RDL {} ../rdl/comcorr.rdl
}

# ==============================================================================
proc doOnCreate {} {
  variable Vhdl
  addSources Vhdl
}

# ==============================================================================
proc doOnBuild {} {
}

# ==============================================================================
proc setSim {} {
}
